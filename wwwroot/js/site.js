﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your Javascript code.

function SetState(guid, state) {
    $.get("/Conference?state=" + state + "&guid=" + guid, function (data, status) {
        location.reload();
    });
}

function copyToClipboard(field) {
    let textarea = document.createElement('textarea');
    textarea.id = 't';
    textarea.style.height = "0";
    document.body.appendChild(textarea);
    textarea.value = field.innerText;
    let selector = document.querySelector('#t');
    selector.select();
    document.execCommand('copy');
    document.body.removeChild(textarea);
}

$(function () {
    $('[data-toggle="tooltip"]').tooltip()
});