using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using c3stream.Pages;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Hosting;

namespace c3stream {
	public static class c3stream {
		public const  string DataPath  = "data";
		public const  string DbFile    = "c3stream.user.json";
		public const  string CachePath = "/mnt/storage/archive/Video/congress/";
		public const  string CacheUrl  = "https://c3stream-mirror.prod.zotan.network/";
		public static object Lock      = new object();
		public static string DbPath    = Path.Combine(DataPath, DbFile);

		public static List<ConferenceObject> Conferences = new List<ConferenceObject> {
			new ConferenceObject("36c3", true),
			new ConferenceObject("camp2019"),
			new ConferenceObject("35c3"),
			new ConferenceObject("34c3"),
			new ConferenceObject("33c3"),
			new ConferenceObject("32c3"),
			new ConferenceObject("31c3"),
			new ConferenceObject("30c3")
		};

		public static void Main(string[] args) {
			if (!Directory.Exists(DataPath))
				Directory.CreateDirectory(DataPath);
			if (!File.Exists(DbPath))
				ConferenceModel.WriteUserData();

			foreach (var conference in Conferences)
				UpdateConference(conference);

			if (args.Length != 0) {
				if (args[0] == "logo")
					foreach (var conference in Conferences) {
						Console.WriteLine($"wget {conference.LogoUri} -O {Path.Combine(CachePath, conference.Acronym, "logo.png")}");
					}
				else if (Conferences.All(p => p.Acronym != args[0]))
					Console.WriteLine("No matching conference found.");
				else
					foreach (var talk in Conferences.First(p => p.Acronym == args[0]).Talks)
						Console.WriteLine($"youtube-dl -f \"best[ext = mp4]\" {talk.FrontendLink} -o \"{Path.Combine(CachePath, args[0], talk.Slug)}.mp4\"");
			}
			else {
				CreateHostBuilder(args).Build().Run();
			}
		}

		public static void UpdateConference(ConferenceObject conference) {
			using var wc = new WebClient();

			var jsonpath = Path.Combine(DataPath, conference.Acronym + "_index.json");
			var json     = "";
			if (!File.Exists(jsonpath)) {
				json = wc.DownloadString($"https://api.media.ccc.de/public/conferences/{conference.Acronym}");
				File.WriteAllText(jsonpath, json);
			}
			else if (conference.Ongoing) {
				json = wc.DownloadString($"https://api.media.ccc.de/public/conferences/{conference.Acronym}");
			}
			else {
				json = File.ReadAllText(jsonpath);
			}

			var parsed = Conference.FromJson(json);
			lock (Lock) {
				conference.Talks.Clear();
				conference.LogoUri = parsed.LogoUrl.AbsoluteUri;
				conference.Talks.AddRange(parsed.Events);
				conference.Talks.ForEach(p => p.Guid = p.Guid.Trim());
			}
		}

		public static void UpdateCookie(HttpRequest resquest, HttpResponse response, string redirectUri) {
			//if new bookmark is in uri
			if (resquest.Query.ContainsKey("bookmark") && resquest.Cookies["bookmark"] != resquest.Query["bookmark"]) {
				response.Cookies.Append("bookmark", resquest.Query["bookmark"], new CookieOptions {Expires = DateTimeOffset.MaxValue});
				response.Redirect(redirectUri + "bookmark=" + resquest.Query["bookmark"]);
			}
			//if no cookie exists or cookie is invalid
			else if (!resquest.Cookies.ContainsKey("bookmark") || !Guid.TryParseExact(resquest.Cookies["bookmark"], "D", out _)) {
				var guid = Guid.NewGuid().ToString();
				response.Cookies.Append("bookmark", guid, new CookieOptions {Expires = DateTimeOffset.MaxValue});
				response.Redirect(redirectUri + "bookmark=" + guid);
			}
			//redir to cookie
			else if (!resquest.Query.ContainsKey("bookmark")) {
				response.Redirect(redirectUri + "bookmark=" + resquest.Cookies["bookmark"]);
			}
		}

		public static Event GetEventByGuid(string guid) {
			return Conferences.SelectMany(c => c.Talks.Where(talk => talk.Guid.ToString() == guid)).FirstOrDefault();
		}

		public static ConferenceObject GetConferenceByEventGuid(string guid) {
			return Conferences.FirstOrDefault(c => c.Talks.Any(t => t.Guid.ToString() == guid));
		}

		public static IHostBuilder CreateHostBuilder(string[] args) =>
			Host.CreateDefaultBuilder(args).ConfigureWebHostDefaults(webBuilder => { webBuilder.UseStartup<Startup>(); });

		public class ConferenceObject {
			public string      Acronym;
			public bool        Ongoing;
			public string      LogoUri;
			public List<Event> Talks = new List<Event>();

			public ConferenceObject(string acronym, bool ongoing = false) {
				Acronym = acronym;
				Ongoing = ongoing;
			}
		}
	}
}
