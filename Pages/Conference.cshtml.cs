﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace c3stream.Pages {
	public class ConferenceModel : PageModel {
		public static    List<UserStatus>         UserData = new List<UserStatus>();
		private readonly ILogger<ConferenceModel> _logger;

		public ConferenceModel(ILogger<ConferenceModel> logger) => _logger = logger;

		public void OnGet() {
			var guid   = Request.Query["guid"];
			var state  = Request.Query["state"];
			var userid = Request.Cookies["bookmark"];
			if (string.IsNullOrWhiteSpace(guid) || string.IsNullOrWhiteSpace(state) || !Request.Cookies.ContainsKey("bookmark"))
				return;

			lock (c3stream.Lock) {
				ReadUserData();
				var existing = UserData.FirstOrDefault(p => p.TalkId == guid && p.UserId == userid);
				if (existing != null)
					if (state == "unwatched")
						UserData.Remove(existing);
					else
						existing.State = state;
				else
					UserData.Add(new UserStatus(userid, guid, state));
				WriteUserData();
				Response.Redirect("/");
			}
		}

		public static void ReadUserData() {
			lock (c3stream.Lock)
				UserData = JsonConvert.DeserializeObject<List<UserStatus>>(System.IO.File.ReadAllText(c3stream.DbPath));
		}

		public static void WriteUserData() {
			lock (c3stream.Lock)
				System.IO.File.WriteAllText(c3stream.DbPath, JsonConvert.SerializeObject(UserData));
		}

		public class UserStatus {
			public readonly string TalkId;
			public readonly string UserId;
			public          string State;

			public UserStatus(string userId, string talkId, string state = "unwatched") {
				UserId = userId;
				State  = state;
				TalkId = talkId;
			}
		}
	}
}