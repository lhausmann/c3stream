﻿using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;

namespace c3stream.Pages {
	public class InfoModel : PageModel {
		private readonly ILogger<InfoModel> _logger;

		public InfoModel(ILogger<InfoModel> logger) => _logger = logger;

		public void OnGet() { }
	}
}